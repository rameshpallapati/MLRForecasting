import { MLRWebPage } from './app.po';

describe('mlrweb App', () => {
  let page: MLRWebPage;

  beforeEach(() => {
    page = new MLRWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

import {Component} from '@angular/core';

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
  
})

export class SearchComponent {
    
    selectedValue: string;

  foods = [
    {value: 'steak-0', viewValue: 'AR'},
    {value: 'pizza-1', viewValue: 'ARMI'}
   
  ];
}

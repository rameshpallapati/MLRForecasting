# MLR Forecasting
  

# Ownership
This project is currently owned by [Ramesh Pallapati] 

Committers with push permissions:

- [Ramesh Pallapati](mailto:pallapati.ramesh@gmail.com)


## Issue Tracking
This project uses [JIRA] for issue-tracking purpose. 

# Setup
```bash
# Install node.js v 6.2.2 from below link  (should be latest or greater than 5.0)
    https://nodejs.org/en/
    
# Install gulp (build task runner) globally 
    npm  install –g  gulp
    
# Install Karma (unit testing task runner) globally 
    npm install –g karma
    
# Install Protractor (end to end testing) globally 
    npm install –g protractor
    
# update selenium webdriver (used by protractor)
    webdriver-manager update
```

# Running Locally
```bash
# Clone the repo 
    git clone <repo-url>
    
# Install the dependencies 
    npm install
    
# Running gulp (build task runner)
    gulp
    
# Running unit test cases with karma
    npm run test
    
# Run e2e test cases 
    protractor protractor.conf.js
```

# Usage



# Contributing


# Changelog


## Version 1

